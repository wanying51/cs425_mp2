package main

import(  
    "fmt"  
    "os"  
    "net"
    "regexp"
   //"bufio"
//"io/ioutil"
    "strings"
    "math/rand"
    "os/exec" 
    "bytes"
   // "encoding/json"
   //"strconv"
   "time"
   "sync"
)

// type TransacInfo struct{
//     Timestamp string
//     ID string
//     SourceAcc string
//     DestAcc string
//     TransacAmount string
// }

// type TransacMsg struct{ 
//     Trasac TransacInfo
//     ReceiveTime time.Time
// }

var transacList = make(map[string]bool) //key: "transac id", value: true

//keep a list of all alive nodes 
var nodeList = make(map[string]string) //key:address value:name
var bytesReceived int
var m sync.Mutex


func main() {
    // To check the input
    checkArg()
    name := os.Args[1]
    hostname, err := os.Hostname()
    port := os.Args[2]
    outFile := os.Args[3]

    outPath := fmt.Sprintf("%s_%s_%s",hostname,port,outFile)
    f, err := os.Create(outPath)
    check(err)

    defer f.Close()
    bytesReceived = 0
    //defer writeByte(f)

    //hardcode service address
    serviceAddr := "172.22.158.25:8888"
    ip := getdns(hostname)
    localAddress := fmt.Sprintf("%s:%s",ip,port)
    nodeList[localAddress] = name
    introduceMe := fmt.Sprintf("INTRODUCE %s %s %s\n",name,ip,port)

    //fmt.Println(fmt.Sprintf("INTRODUCE me: %s",introduceMe))
    //Connect to Service
    conn, err := net.DialTimeout("tcp",serviceAddr,2 * time.Second)
    //defer conn.Close()
    if err != nil {
        //fmt.Println("Connect to service error")
        return
    }
    connectCommand := fmt.Sprintf("CONNECT %s %s %s\n",name,ip,port)
    conn.Write([]byte(connectCommand))
    startTime := float64(time.Now().UnixNano())/1000000000.0
    f.WriteString(fmt.Sprintf("START %.6f\n",startTime))

    //Set up a Server
    server := setupServer(port)

    //Handle incoming commands
    //var wg sync.WaitGroup

    go func(){
        for{
            connA, err := server.Accept()
            //defer connA.Close()
            errHandler(err, "#Can not open connection!", true)
            if err != nil {
                return
            }   
            //wg.Add(1)   
            go connHandler(f,connA,introduceMe)
        }
    }()
    //wg.Add(1)   
    go connHandler(f,conn,introduceMe)
    //wg.Add(1)   
    go nodeDiscover(localAddress) 
    // fmt.Scanln()
    //wg.Wait()
    fmt.Scanln()
    fmt.Println("Done")
}

func writeByte(f *os.File){
    //fmt.Println("WriteByte")
    f.WriteString(fmt.Sprintf("Received Bytes: %d\n",bytesReceived))
}

func connHandler(f *os.File,conn net.Conn,introduceMe string){   
    defer conn.Close()
    for{
        inCommand := make([]byte, 512)
        size, err:=conn.Read(inCommand)
        receiveTime := float64(time.Now().UnixNano())/1000000000.0
        if err != nil{
            return
        }    

        bytesReceived += size
        //fmt.Println("Size:", size)  
        //fmt.Println("Bytes Size:", bytesReceived)                                                                                        
        commandMsgs := strings.Split(string(inCommand[:size]),"\n")
        for _,commandMsg := range commandMsgs{
            if isIntroduce(commandMsg){   
                args := strings.Split(commandMsg," ")
                nodeName, nodeIP, nodePort := args[1], args[2], args[3]
                nodeAddress := fmt.Sprintf("%s:%s",nodeIP,nodePort)
                _, ok := nodeList[nodeAddress]
                if(ok == false) { // If newly introduced, then keep it in nodeList and introduce itself back
                    nodeList[nodeAddress] = nodeName
                    connA, err := net.DialTimeout("tcp", nodeAddress,2 * time.Second)
                    //defer connA.Close()
                    if err != nil {
                        //fmt.Println("Connect to dest error:",err)
                        return
                    }
                    //fmt.Println(commandMsg)
                    connA.Write([]byte(introduceMe))
                }   
                //fmt.Println(nodeList)
            }else if isTransaction(commandMsg) {
                args := strings.Split(commandMsg," ")
                //transacTime,transacID,transacSourceAcc,transacDestAcc,transacAmount := args[1], args[2], args[3],args[4],args[5]
                transacID := args[2]
                
                m.Lock()
                _, ok := transacList[transacID]
                if(ok == false ) { // If newly introduced, then keep it in nodeList and introduce itself back
                    //log transaction
                    //fmt.Println("transacID:",transacID)
                    logMsg := fmt.Sprintf("%s %.6f\n",transacID, receiveTime)
                    f.WriteString(logMsg)

                    f.Sync()

                    //transacInfo := TransacInfo{transacTime,transacID,transacSourceAcc,transacDestAcc,transacAmount}
                    //transacMsg := TransacMsg{transacInfo,time.Now()}
                    transacList[transacID] = true
                    gossipMsg := fmt.Sprintf("%s\n",commandMsg)
                    gossipTransac(gossipMsg,5)
                }  
                m.Unlock()              
            }else if isDiscover(commandMsg){
                //fmt.Println("COMMAND:",commandMsg)
                args := strings.Split(commandMsg," ")
                nodeAddress := args[1]
                connB, err := net.DialTimeout("tcp",nodeAddress,2 * time.Second)
                if err != nil {
                    //fmt.Println("Connect to query node error:",err)
                    return
                }
                for introduceAddress, introduceName := range nodeList{
                     introduceInfo:= strings.Split(introduceAddress,":")
                     introduceIP, introducePort := introduceInfo[0],introduceInfo[1]
                    introduceCommand := fmt.Sprintf("INTRODUCE %s %s %s\n",introduceName,introduceIP,introducePort)
                    connB.Write([]byte(introduceCommand))
                }
            }else if isDie(commandMsg){
                fmt.Println("DIE")
                //f.WriteString(fmt.Sprintf("Received Bytes: %d\n",bytesReceived))
                endTime := float64(time.Now().UnixNano())/1000000000.0
                f.WriteString(fmt.Sprintf("END %.6f\n",endTime))
                os.Exit(0)
            }
        }
    }
}

func selRand() (k string, v string) { //get 
    rand.Seed(time.Now().UnixNano())
    i := rand.Intn(len(nodeList))
    for k := range nodeList {
        if i == 0 {
            return k, nodeList[k]
        }
        i--
    }
    panic("never")
}

func gossipTransac(commandMsg string, num int){
    for i:=0;i < num; i++{
        target,_ := selRand()

        conn, err := net.DialTimeout("tcp", target,2 * time.Second)
        if err != nil {
            //fmt.Println("Connect to gossip dest error:",err)
            return
        }
        conn.Write([]byte(commandMsg))
    }
    return
}

    
func check(e error) {
    if e != nil {
        panic(e)
    }
}

func nodeDiscover(myAddress string){
    for{
        for address, _ := range nodeList{
        if(strings.Compare(myAddress,address)!=0&&checkConnect(address)){
                conn, err := net.DialTimeout("tcp",address,2 * time.Second)
                //defer connA.Close()
                if err != nil {
                    //fmt.Println("Connect to discover node error:",err)
                    return
                }
                discoverCommand := fmt.Sprintf("DISCOVER %s\n",myAddress)
                conn.Write([]byte(discoverCommand))
            }
        }
        time.Sleep(100 * time.Millisecond) //0.1s
    }
}

func checkConnect(target string) bool{
    _, err := net.DialTimeout("tcp", target, 100 * time.Millisecond)
    if err != nil {
        _, ok := nodeList[target]
        if(ok == true) {// if cannot dial, then print left information
            //fmt.Println(nodeList[target] + " has left")
            delete(nodeList, target)
        }
        return false
    }
    return true
}

func setupServer(port string) net.Listener {  //set up listener for the port
    server, err := net.Listen("tcp", ":" + port)
    errHandler(err, "#Can not start server!", true)
    fmt.Println("Start server")
    return server
}

func isIntroduce(command string)bool{
    isIntro,_ := regexp.Match("INTRODUCE.",[]byte(command))
    return isIntro
}

func isTransaction(command string)bool{
    isTransac,_ := regexp.Match("TRANSACTION.",[]byte(command))
    return isTransac
}

func isDiscover(command string)bool{
    isDiscover,_ := regexp.Match("DISCOVER.",[]byte(command))
    return isDiscover
}

func isDie(command string)bool{
    isDie,_ := regexp.Match("DIE",[]byte(command))
    return isDie
}

// Get DNS of the host
func getdns(vm string) string{
    cmd := exec.Command("/usr/bin/dig","+short", vm)
    resultsBytes, err := cmd.CombinedOutput()
    resultsBytes = bytes.Trim(resultsBytes, "\n")
    errHandler(err,"#Couldn't lookup machine address:" + vm, true )
    return string(resultsBytes)
}


func checkArg() {
    if len(os.Args) != 4  {     
        fmt.Println("#Usage: " + os.Args[0] + " <name> <port> <out_file>")  
        os.Exit(0)  
    }
}

// handle the error message and print to the terminal
func errHandler(err error, message string, exit bool) {
    if err != nil {
        fmt.Println(message)
        if exit {
            os.Exit(1)
        }
    }
}